<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-danger">
  <a class="navbar-brand" href="{{url('/')}}">CantabriaSkills 2020</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a href="{{url('/modalidades')}}" class="nav-link {{ Request::is('modalidades*') && !Request::is('participantes/inscribir')? ' active' : ''}}">Modalidades</a>
      </li>
      <li class="nav-item">
        <a href="{{url('/participantes/inscribir')}}" class="nav-link {{ Request::is('participantes/inscribir')? ' active' : ''}}">Inscribir participante</a>
      </li>
    </ul>
    
  </div>
</nav>





