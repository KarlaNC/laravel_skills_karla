<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modalidad;
use App\Participante;
use Illuminate\Support\Facades\Storage;

class ModalidadesController extends Controller
{
    public function getTodas(){
    	$modalidades = Modalidad::all();
		return view('modalidades.index', array('arrayModalidades' => $modalidades)); 
	}

	public function getVerModalidad($slug){ 
    	$modalidadesSeleccionadas = Modalidad::where('slug',$slug)->get();//->first();
		return view('modalidades.mostrar',  array('arrayModalidades' => $modalidadesSeleccionadas));
	}

}
