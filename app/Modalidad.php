<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * @property string $slug
 * @property string $nombre
 * @property string $familiaProfesional
 */
class Modalidad extends Model
{
    protected $table = "modalidades";

    public function participantes(){
		return $this->hasMany("App\Participante");
	}
}
