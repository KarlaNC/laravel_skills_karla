<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modalidad;
use App\Participante;
use Illuminate\Support\Facades\Storage;

class ParticipantesController extends Controller
{
	public function puntuar($slug){

		$arrayModalidades = Modalidad::where('slug',$slug)->get();
		$modalidad_id = Modalidad::select('id')->where('slug',$slug)->pluck('id');
		$participantes = Participante::where('modalidad_id',$modalidad_id)->get();

		foreach ($participantes as $p) {
			$p->puntos = random_int(0, 100);
			$p->save();
		}

		return view('modalidades.mostrar',array('arrayModalidades' => $arrayModalidades));
	}

	public function resetear($slug){
		$arrayModalidades = Modalidad::where('slug',$slug)->get();
		$modalidad_id = Modalidad::select('id')->where('slug',$slug)->pluck('id');
		$participantes = Participante::where('modalidad_id',$modalidad_id)->get();

		foreach ($participantes as $p) {
			$p->puntos = -1;
			$p->save();
		}
		return view('modalidades.mostrar',array('arrayModalidades' => $arrayModalidades));
	}


	public function getInscribir(){
		$arrayModalidades = Modalidad::all();
		return view('participantes.crear', array('arrayModalidades' => $arrayModalidades));
	}

	public function postInscribir(Request $request){
		
		$mascota = new Participante();

		$mascota->nombre = $request->nombre; 
		$mascota->apellidos =  $request->apellidos;
		$mascota->centro =  $request->centro;
		$mascota->tutor =  $request->tutor;
		$mascota->fechaNacimiento =  $request->fechaNac; //Poner el nombre exacto del html
		$mascota->imagen =  $request->imagen->store( "",'participante'); // 
		$p->modalidad_id = Modalidad::where('nombre','like',"%web%")->first()->id;
    	$p->save();
		
		// $p = new Participante();
  //   	$p->nombre = "Andrés";
  //   	$p->apellidos = "Trozado";
  //   	$p->centro = "IES Augusto González de Linares";
  //   	$p->tutor = "Lola Mento";
  //   	$p->fechaNacimiento = "2000-01-01";
  //   	$p->imagen = "participante-11.png";
  //   	$p->modalidad_id = Modalidad::where('nombre','like',"%web%")->first()->id;

		try {
			$mascota->save();
			return redirect("modalidades")->with("mensaje", "Creado con exito");
		} catch (Exception $ex) { //\Illuminate\Database\QueryException
			return redirect("modalidades")->with("mensaje", "Fallo al crear la mascota");
		}

		//return view('mascotas.crear');
	}

   
}
