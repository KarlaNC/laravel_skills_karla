@extends('layouts.master')

@section('titulo')
    Crear
@endsection

@section('contenido')
<div class="row">
  <div class="offset-md-3 col-md-6">
    <div class="card">
      <div class="card-header text-center">
        Añadir mascota
      </div>
    <div class="card-body" style="padding:30px">
  {{-- TODO: Abrir el formulario e indicar el método POST --}}
    <form action="{{action('ParticipantesController@postInscribir') }}" method="post" enctype="multipart/form-data"> {{--url('mascotas/crear')--}}
      {{ csrf_field() }}
      {{-- TODO: Protección contra CSRF --}}
        <div class="form-group">
          <label for="nombre">Nombre</label>
          <input type="text" name="nombre" id="nombre" class="form-control">
        </div>
        <div class="form-group">
        {{-- TODO: Completa el input para la especie --}}
        <label for="apellidos">Apellidos</label>
          <input type="text" name="apellidos" id="apellidos" class="form-control">
        </div>
        <div class="form-group">
        {{-- TODO: Completa el input para la raza --}}
        <label for="centro">Centro</label>
          <input type="text" name="centro" id="centro" class="form-control">
        </div>
        <div class="form-group">
        {{-- TODO: Completa el input para la fecha de nacimiento --}}
        <label for="fechaNac">Fecha nacimiento</label>
          <input type="date" name="fechaNac" id="fechaNac" class="form-control">
        </div>
        <div class="form-group">
        {{-- TODO: Completa el input para el cliente --}}
        <label for="modalidad">Modalidad</label>
          <select name="modalidad">
          @foreach ($arrayModalidades as $modalidad)
            <option name="modalidad"> {{ $modalidad->nombre }}</option>
          @endforeach
          </select>
        </div>
        <div class="form-group">
          {{-- TODO: Completa el input para la imagen --}}
          <label for="imagen">Imagen</label>
          <input name="imagen" type="file" class="btn btn-outline-dark" />
        </div>
        <div class="form-group text-center">
          <button type="submit" class="btn btn-outline-dark" >Añadir participante</button>
        </div>
    </form>
    {{-- TODO: Cerrar formulario --}}
    </div>
    </div>
  </div>
</div>
@endsection