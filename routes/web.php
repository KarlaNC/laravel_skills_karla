<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@getInicio'); // redirigir modalidades
Route::get('modalidades', 'ModalidadesController@getTodas');

Route::get('modalidades/mostrar/{slug}', 'ModalidadesController@getVerModalidad');
Route::get('modalidades/puntuar/{slug}', 'ParticipantesController@puntuar');
Route::get('modalidades/resetear/{slug}', 'ParticipantesController@resetear');

Route::get('participantes/inscribir', 'ParticipantesController@getInscribir');
Route::post('participantes/inscribir', 'ParticipantesController@postInscribir');

Route::any('api', 'SoapServerController@getServer');//post-get -> any

Route::any('api/wsdl', 'SoapServerController@getWSDL');

