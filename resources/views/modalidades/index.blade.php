@extends('layouts.master')

@section('titulo')
	Index
@endsection

@section('contenido')

	@if (session('mensaje'))
		<div class="alert alert-success" role="alert">
		  {{ session('mensaje') }}
		</div>
	@endif

	<div class="row">
		@foreach( $arrayModalidades as $modalidad )
			<div class="col-xs-12 col-sm-6 col-md-4 border">
				<img class="border border-light" src="{{ asset('assets/imagenes/modalidades')}}/{{ $modalidad->imagen }}" style="height:120px"/>
				<div style="margin-left: 10px;">
					
					<a href="{{ url('modalidades/mostrar/' ) }}/{{$modalidad->slug}}">
						<h4 style="min-height:45px;margin:auto ">
						{{ $modalidad->nombre }}
						</h4>
					</a>
					<p>{{ $modalidad->especie }}</p>

					<p>{{ count($modalidad->participantes) }}</p>
				</div>
			</div>
		@endforeach
	</div>

@endsection