<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use SoapServer;
use App\Modalidad;
use App\Participante;
use App\lib\WSDLDocument;
use Illuminate\Http\Request;

class SoapServerController extends Controller
{
   	private $clase = "App\\HTTP\\Controllers\\SkillsWebService";
	private $urlWSDL = "C:/xampp/htdocs/Examen/laravel_skills_karla/public/api/wsdl";
	private $uri = "C:/xampp/htdocs/Examen/laravel_skills_karla/public/api";

    public function getServer() {
		$server = new SoapServer(null,array('uri'=>$uri));
		$server->setClass("App\\HTTP\\Controllers\\SkillsWebService");
		$server->handle();

    }
   
    public function getWSDL() {
    	$wsdl = new WSDLDocument($this->clase, $this->uri, $this->uri);
    	$wsdl->formatOutput = true;
		header('Content-Type: text/xml');
		echo $wsdl->saveXML();
    }
}

class SkillsWebService
{
	/**
	 * Description
	 * @param string $centro 
	 * @return int
	 */
	function getNumeroParticipantesCentro($centro){
		$participantes = Participante::where('centro',$centro)->get();
		return count($participantes);
	}

	/**
	 * Description
	 * @param string $tutor 
	 * @return App\Participante[]
	 */
	function getNumeroParticipantesTutor($tutor){
		$arrayParticipantes = Participante::where('tutor',$tutor)->orderBy('puntos')->get();//, 'desc'
		return $arrayParticipantes;
	}
}
// ->orderBy('name', 'desc')->groupBy('count')->having('count', '>', 100)->get();