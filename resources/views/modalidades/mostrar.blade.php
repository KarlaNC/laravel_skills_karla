@extends('layouts.master')

@section('titulo')
	Index
@endsection

@section('contenido')

	@if (session('mensaje'))
		<div class="alert alert-success" role="alert">
		  {{ session('mensaje') }}
		</div>
	@endif

	<div class="row">
		@foreach( $arrayModalidades as $modalidad )
			<div class="col-md-12">
				<h3>{{ $modalidad->nombre }}</h3>
				<h4> Familia profesional : <span>{{ $modalidad->familiaProfesional }}</span></h4>
				<h4> Participantes :</h4>
			</div>
			{{-- <div class="col-xs-12 col-sm-6 col-md-4 border"> --}}
			@foreach ($modalidad->participantes as $participante)
				<div class="col-md-4">
				 	<p>{{ $participante->nombre }}</p>
					<img class="border border-light" src="{{ asset('assets/imagenes/participantes')}}/{{ $participante->imagen }}" style="height:120px"/>
				</div>
			@endforeach 
			<div class="col-md-12"></div>
			<div class="col-md-6">
				<a href="{{ url('modalidades/puntuar/') }}/{{ $modalidad->slug}}" type="button" class="btn btn-primary " > Puntuar </a>
				<a href="{{ url('modalidades/resetear/') }}/{{ $modalidad->slug }}" type="button" class="btn btn-primary " > Resetear </a>	
			</div>	
			@foreach ($modalidad->participantes as $participante)	
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-stripe">
						    <thead class="thead-light">
						      <tr>
						        <th>Nombre</th>
						        <th>Puntos</th>
						      </tr>
						   	</thead> 
						   	<p></p>
						   	@if($participante->puntos != -1)
							   	<tr>
							        <td>{{ $participante->nombre }}</th>
							        <td>{{ $participante->puntos }}</th>
							    </tr>
							@endif     
						</table>
					 </div>
				</div>
			@endforeach 
		@endforeach
	</div>

@endsection